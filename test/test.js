'use strict';

const chai = require('chai');
chai.use(require('chai-as-promised'));
const expect = chai.expect;
const middleware = require('../index');
const koa = require('koa');
const rp = require('request-promise');

describe('Test', ()=> {
  var server;
  var req = rp.defaults({baseUrl: 'http://localhost:3000/', json: true});
  var app = koa();
  var router = require('koa-router')();

  router.get('/obj', middleware({
    filterCallback: (key, val) => {
      return val === 2;
    }
  }), function*() {
    this.response.body = {a: 1, b: 1, c: 2};
  });
  router.get('/arr', middleware({
    filterCallback: (key, val) => {
      return val === 2;
    }
  }), function*() {
    this.response.body = [{a: 2, b: 1, c: 1}, {a: 1, b: 2, c: 2}, {a: 1, b: 1, c: 2}];
  });
  router.get('/obj_recursive', middleware({
    filterCallback: (key, val) => {
      return val !== 2;
    }
  }), function*() {
    this.response.body = {a: 1, b: 1, c: {c1: 1, c2: 2}};
  });
  router.get('/arr_recursive', middleware({
    filterCallback: (key, val) => {
      return val !== 2;
    }
  }), function*() {
    this.response.body = [{a: 1, b: 1, c: {c1: 1, c2: 2}}, {a: 1, b: 2, c: {c1: 1, c2: 2}}];
  });
  router.get('/obj_empty', middleware({
    filterCallback: (key, val) => {
      return val !== 2;
    }
  }), function*() {
    this.response.body = {a: 1, b: 1, c: {c1: 2}};
  });
  router.get('/arr_empty', middleware({
    filterCallback: (key, val) => {
      return val !== 2;
    }
  }), function*() {
    this.response.body = {a: 1, b: 1, c: [{c1: 2}]};
  });
  router.get('/obj_root_empty', middleware({
    filterCallback: (key, val) => {
      return val !== 2;
    }
  }), function*() {
    this.response.body = {a: 2};
  });
  router.get('/arr_root_empty', middleware({
    filterCallback: (key, val) => {
      return val !== 2;
    }
  }), function*() {
    this.response.body = [{a: 2}];
  });

  app.use(router.routes());
  app.use(router.allowedMethods());

  before(done=> {
    server = app.listen(3000, done);
  });
  after(done=> {
    server.close(done);
  });

  it('should only keep the fields that the filterCallback returned true for', ()=> {
    return expect(req('/obj')).to.eventually.deep.equal({c: 2});
  });
  it('should be able to filter arrays', ()=> {
    return expect(req('/arr')).to.eventually.deep.equal([{a: 2}, {b: 2, c: 2}, {c: 2}]);
  });
  it('should be able to filter objects recursively', ()=> {
    return expect(req('/obj_recursive')).to.eventually.deep.equal({a: 1, b: 1, c: {c1: 1}});
  });
  it('should be able to filter arrays recursively', ()=> {
    return expect(req('/arr_recursive')).to.eventually.deep.equal([{a: 1, b: 1, c: {c1: 1}}, {a: 1, c: {c1: 1}}]);
  });
  it('should drop a field if the field is an empty object after filter', ()=> {
    return expect(req('/obj_empty')).to.eventually.deep.equal({a: 1, b: 1});
  });
  it('should drop a field if the field is an empty array after filter', ()=> {
    return expect(req('/arr_empty')).to.eventually.deep.equal({a: 1, b: 1});
  });
  it('should not drop the empty object if it is root', ()=> {
    return expect(req('/obj_root_empty')).to.eventually.deep.equal({});
  });
  it('should not drop the empty array if it is root', ()=> {
    return expect(req('/arr_root_empty')).to.eventually.deep.equal([]);
  });
  it('should set the filterCallback to a function that always return true if it is missing in the opt', ()=> {
    var opt = {};
    middleware(opt);
    expect(opt.filterCallback).to.exist;
    expect(opt.filterCallback).to.be.an.instanceof(Function);
    expect(opt.filterCallback.toString()).to.eq('()=>true');
  });
  it('should throw if the filterCallback is not a function', ()=> {
    expect(()=> {
      // noinspection JSCheckFunctionSignatures
      middleware({filterCallback: 'not a function'});
    }).to.throw('filter callback must be a function');
  });
});