'use strict';

var _ = require('lodash');

/**
 * filter callback, returns true if you want to keep the property
 *
 * @callback filterCallback
 * @param {String} key - the key of the object property
 * @param {*} val - the value of the object property
 * @returns {Boolean} - true to keep the property, false to drop the property
 */

/**
 *
 * @param {Array|Object} target - the array or object to filter
 * @param {filterCallback} filterCallback
 * @returns {Array|Object}
 */
function filterRecursive(target, filterCallback) {
  if (_.isArray(target)) {
    var filteredArray = _.map(target, target=>filterRecursive(target, filterCallback));
    return _.reject(filteredArray, _.isEmpty);
  } else if (_.isPlainObject(target)) {
    var obj = {};
    _.forOwn(target, (val, key)=> {
      if (filterCallback(key, val)) {
        var filteredVal = filterRecursive(val, filterCallback);
        if (!((_.isArray(filteredVal) || _.isPlainObject(filteredVal)) && _.isEmpty(filteredVal))) {
          obj[key] = filteredVal;
        }
      }
    });
    return obj;
  } else {
    return target;
  }
}

/**
 *
 * @param {Object} [opt={}] - options
 * @param {filterCallback} [opt.filterCallback=()=>true] - filter callback, return true if you want to keep the property
 * @returns {Function}
 */
module.exports = function(opt) {
  opt = opt || {};
  if (!opt.filterCallback) {
    opt.filterCallback = ()=>true;
  }
  if ('function' !== typeof opt.filterCallback) {
    throw new Error('filter callback must be a function');
  }

  /**
   * @this {koa.Context}
   */
  return function*(next) {
    yield next;
    if (_.isArray(this.body) || _.isPlainObject(this.body)) {
      this.body = filterRecursive(this.body, opt.filterCallback);
    }
  };
};
